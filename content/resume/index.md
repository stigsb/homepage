---
title: "Stig Bakken's Resume"
aliases:
  - /resume.html
  - /post/resume/
---

## Resume for Stig Sæther Nordahl Bakken

### Work Experience

This is an outline, further details are available on request.

#### Dec 2020-present: [Huawei Cloud](https://www.huaweicloud.com)

**Database Senior Architect**

Design and implementation aspects of two HTAP (hybrid OLTP/OLAP) database products for
use with GaussDB for MySQL (similar to AWS Aurora):

 * Orion: [ClickHouse](https://clickhouse.com)-based solution with replication
   latency of seconds, designed and implemented (in a small team) transaction support, usability
   improvements and performance improvements.
 * Hermes: [DuckDB](https://duckdb.org)-based solution with replication latency of
   milliseconds, designed and implemented metrics/monitoring system and cloud control
   plane integration. Large team.

Technologies involved: C++20, CMake, MySQL, Python, ClickHouse, DuckDB, OpenMetrics, Prometheus,
Docker, Ansible

#### Dec 2019-Nov 2020: [TietoEVRY](https://www.evry.com) Financial Crime Prevention

**Principal Software Engineer**

Primary focus on data engineering for enabling efficient machine
learning workflows for fraud detection.

Work included:

 * Cloud architecture for data warehouse / analytics workloads for the financial services division,
   got ClickHouse approved by the Enterprise Architecture Board.
 * Designed and led small team implementing PCI-connected data mirroring service, providing data
   catalog/governance features as well as aggregating data from Oracle, MySQL, plain CSV files and
   ClickHouse.
 * Tech/tools involved: Java, Kotlin, Python, Oracle, MySQL, ClickHouse, AWS, Azure, Kubernetes

#### May 2009-Nov 2019: [Zedge](https://www.zedge.net/)

**Chief Technology Officer**

Work included:

 * Introduced SCRUM
 * Modernized existing (PHP) codebase, introduced code reviews
 * Containers / cloud migration, initially Mesos/Marathon, later Google Cloud / Kubernetes
 * Continuous integration/deployment (CI/CD) with Jenkins and Mesos/Marathon (2013-2017)
 * Continuous deployment / GitOps with GitLab and Kubernetes (2017-)
 * Internal tools for Continuous Experimentation
 * Mentoring/coaching technical staff, helping with all kinds of troubleshooting
 * Hiring - interviews, talks and meetups
 * Overall architecture and internal dev productivity / collaboration tools and processes
 * Data/platform engineering - led team building a new data pipeline+warehouse for apps/web with focus on developer productivity (DX) and automation.


#### 2006-2009: [Google](https://www.google.com)

**Senior Software Engineer** (Infrastructure and Search Properties)

Work included:

 * Config-driven search (making it easier to repurpose core search)
 * System for elastically scaling a search cluster based on predicted traffic
 * Training data labeling system for Google News article category prediction
 * Updated Google Alerts UI
 * Recruiting - interviews and various talks around Europe


#### 2003-2006: [Yahoo!](https://www.yahoo.com) (acquisition, has since been acquired by Verizon)

**Engineering Manager**, Yahoo! Image Search (2005-2006)

**Technical lead**, Yahoo! Multimedia Search (2003-2004)

Some early contributions to in-house search platform called [Vespa](htts://vespa.ai) which has later
been [open sourced](https://vespa.ai).

#### 2003: [Overture, later Yahoo! Search Marketing](https://en.wikipedia.org/wiki/Yahoo!_Search_Marketing") (acquisition)

Integrated FAST and AltaVista Multimedia Search products


#### 1999-2003: [Fast Search & Transfer](https://en.wikipedia.org/wiki/Microsoft_Development_Center_Norway) (aquisition, has since been acquired by Microsoft)

 * Multimedia Search technical lead (2002-2003)
 * part of [alltheweb.com](https://en.wikipedia.org/wiki/AlltheWeb) frontend team (2001-2002)
 * built first FAST/Lycos Multimedia Search (1999)
 * maintained FAST FTP Search (1999-2003)
 * built and maintained [FAST/Lycos MP3 Search](https://www.computerworld.co.nz/article/515918/lycos_norway_fast_offer_mp3_search/) (1999-2002)
 * contributed to improving PHP performance and functionality for FAST products


#### 1996-1998: Co-founded Guardian Networks

 * main product 1: Hardened Linux distribution for firewalls (Guardian Networks Secure Linux)
 * main product 2: Web publishing platform
 * consulting work, including Unix sysadmin, Oracle, web development and building an academic Linux distribution


#### 1995-1996: [BIBSYS](https://www.bibsys.no) (Norwegian academic library database)

 * Unix systems engineer
   * Unix resource during port of BIBSYS application from VM/ESA to AIX
   * System administration and automation


#### 1994-1995: [NTNU](https://www.ntnu.no) IT department

 * internal Unix/networking training
 * built NTNU's first centralized Unix account database
 * maintained heterogenous Unix software distribution system (Store)


#### 1993: [NTNU](https://www.ntnu.no) Institute for Petroleum Technology

 * summer internship installing AIX student lab


### Publications

 * [PHP 5 Power Programming](http://www.amazon.com/dp/013147149X) (Bruce Perens' Open Source Series)
   Prentice Hall 2004, 720 pages

   ISBN 0-13-147149-X (Published translations: Polish, German, Russian, Italian, Czech)


### Internet / Open Source "claims to fame"

 * 1994: Implemented first web interface to MS160 Archie, later known as FTP Search
 * 1996-2003: Active member of the [PHP Group](https://www.php.net/credits.php), contributed support for Oracle, XML, DocBook-based documentation framework (still in use), Unix build system, extension package format+installer (PECL, still in use), some ODBC drivers, \*printf and much more
 * 1999-2002: Held various PHP-related talks and workshops at PHP/Apache/Linux conferences in Europe/US
 * 2001-2004: Founded [PHP Extension and Application Repository (PEAR)](https://pear.php.net) and built the initial infrastructure - later retired and handed off the project to [The PEAR Group](https://pear.php.net/group/)
 * [Apache Software Foundation](https://www.apache.org) [Emeritus](https://www.apache.org/foundation/members.html#emeritus-members-of-the-apache-software-foundation)


### Nominations and Awards

 * 1999, Nominated: [GNU Free Software Award 1999](https://www.gnu.org/award/award-1999.html") for PHP work
 * 2002, Nominated: [the NUUG (.no USENIX branch) award](http://nuug.no/prisen/kandidater-2002/) for PHP work
 * 2002: Nominated: ActiveState Active Award 2002 for PHP/PEAR work
 * 1996: Won: VG (major Norwegian newspaper) "most useful web site" award for FTP Search
 * 2003: Won: [ActiveState Active Award (Programmers Choice) 2003](http://www.activestate.com/company/newsroom/press/2003_07_09_0) for [PEAR](http://pear.php.net)
 * 2004: Won: [Search Engine Watch award 2004 for best Image search engine](http://searchenginewatch.com/showPage.html?page=3494141#image) (Google came second)

___

[**PDF Version**](CV_Stig_Bakken.pdf)

